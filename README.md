#Place ACF coords onto a Google map in Wordpress

A Wordpress plugin that places the map locations entered using Advanced Custom Fields onto a Google Map.

##Shortcode

[hssmapper width="550px" height="400px" type="person,building" geofield="location" zoom="2"]

###Required attributes

* 'type' - a comma-seperated list of the (custom) post-types name that have the ACF location data and that you wish to appear on the map
* 'geofield' - the name of the ACF field that contains the map coordinates

###Other attributes

* 'width' & 'height' - the width and height values of the map on the page. Include 'px' or '%'
* 'zoom' - the google map zoom value - 1 - 15

###Custom icons

Custom icons should be in png format and named exactly the same as the post-type e.g. person.png or building.png

Place these files into this plugin's /images folder

* [Download from a huge selection of free custom map icons](http://mapicons.nicolasmollet.com/)

If you don't put a properly named custom icon png into the plugins /images folder, the default Google icon will be used in place.

##Dependencies

* [ACF { Location Field](https://github.com/elliotcondon/acf-location-field)
* [Advanced Custom Fields](http://wordpress.org/plugins/advanced-custom-fields/)
  

##Issues with map controls

If you find the controls on your maps are looking 'squashed', then it is probably a CSS issue from the theme you are using. Try adding the css to your child theme:

    .gmnoprint img { max-width: none;}

