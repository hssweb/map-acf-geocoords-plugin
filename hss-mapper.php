<?php
/*
Plugin Name: HSS ACF Location Mapper
Plugin URI: http://www.ed.ac.uk
Description: Provides a shortcode to add custom post type(s) coords in ACF location fields to a Google map
Version: 1.0
Author: Euan Cameron
Author URI: http://putdaveonthemap.com
License: GPLv2
*/

class HSSMapper {

	private $pluginPath;
	private $pluginUrl;
	private $maps = array();
	private $lats = array();
	private $longs = array();

	
	public function __construct()
	{
		// Set Plugin Path
		$this->pluginPath = plugin_dir_path( __FILE__ );
	
		// Set Plugin URL
		$this->pluginUrl = plugin_dir_url( __FILE__ );
		
		//  Do shortcode
		add_shortcode('hssmapper', array($this, 'shortcode')); 	
	}

	/* Initialise shortcode */
	public function shortcode($atts) {
	
		// Extract shortcode attributes
	    extract(shortcode_atts(array(
	        'type' => '',
	        'width' => '550px',
	        'height' => '400px',
	        'geofield' => '',
	        'zoom' => 6,
	    ), $atts));

		// $this->map_data will be passed to javascript
		// Default center == Edinburgh, UK
	    $this->map_data = array(
	        'markers' => array(),
	        'center' => array(
	            55.9531,
	            3.1889
	        ),
	        'zoom' => $zoom
	    );
		
	    // Post type & field are a requirement
	    if (trim($type) == '' || trim($geofield) == '') {
	        return; // Exit without doing anything
	    }
	    
	    // Put types into an array
	    $types = array_map('trim', explode(',', $type));
	    
	    // Put categories into array
	    // $cats = array_map('trim', explode(',', $cat)); 
		    
	    // Set up WP query
	    $args = array(
	        'post_status' => 'publish',
	        'post_type' => $types,
	        'posts_per_page' => -1
	    );
	    

	    $map_query = new WP_Query($args);

    
		// Check is posts returned
	    if ($map_query->have_posts()):
	    
	    // Load scripts when shortcode used
		$this->registerScript();

        // Reset array position counter
        $counter = 0;
        
	    	// Begin Loop		 
	        while ($map_query->have_posts()):
	        
	            $map_query->the_post();
	            
	            $iconName = get_post_type(); // Name icons after content types e.g. journal.png for CPT journal 
                
                $iconURL = $this->pluginUrl . '/images/' . $iconName . '.png'; // same name as your post type
                
                $iconFile = $this->pluginPath . '/images/' . $iconName . '.png';    
	            
	            $location = get_field($geofield); // TODO: what happens if not using the ACF Location Field (EG) addon 
	            
	            if (is_array($location) && count($location) > 0) {
	            
	                $meta_coords = $location['coordinates'];
	
	                if ($meta_coords) {
	                    $coords = array_map('floatval', array_map('trim', explode(',', $meta_coords)));
	                    $title = get_the_title();
	                    $link = sprintf('<a href="%s">%s</a>', get_permalink(), $title);
	                    /* The object to pass to the Javascript file */
	                    $this->map_data['markers'][] = array(
	                        'latlang' => $coords,
	                        'title' => $title,
	                        'desc' => '<h3 class="marker-title">' . $link . '</h3>',
	                    );
                      
                        // Check if post-type.png exists, and use if it does
                        if (file_exists($iconFile)) { 
                        print_r($iconFile);                                                     
                             $this->map_data['markers'][$counter]['icon'] = $iconURL;
                            }
                        
	                    $this->lats[] = $coords[0];
	                    $this->longs[] = $coords[1];
                        
                        $counter += 1; // Increment array postion counter
	                }
	                
	            }
	            
	        endwhile;  // End Loop
   
    				        
	    endif;


		// Make sure we reset the post
	    wp_reset_postdata(); 
		
		// Centre map
		$this->centerCoords();
	    
	    wp_localize_script('posts_map', 'map_data', $this->map_data); // Make the data available to the JS script
	    
	    $mapContainer = '<div id="map" style="width:' . $width . '; height:' . $height . '"></div>';
	    
	    return $mapContainer;
					   			
	}
	
	/* Calculate center */
	private function centerCoords() {
        if (!empty($this->lats)) {
            $this->map_data['center'] = array(
                (max($this->lats) + min($this->lats)) / 2,
                (max($this->longs) + min($this->longs)) / 2
            );
	  }	
	}


	/* Register and Enque scripts */
	private function registerScript() {
		    wp_register_script('google-maps-api', 'http://maps.google.com/maps/api/js?sensor=false', false, false, false);
		    wp_register_script('posts_map', $this->pluginUrl . '/js/pdotmap.js', array(
		        'google-maps-api'
		    ), false, true);
		    // Enque the scrips
		    wp_enqueue_script('google-maps-api');
		    wp_enqueue_script('posts_map');
		}

}

$HSSMapper = new HSSMapper();